import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  /*title = "employee-module3";
  title2 = "pIpE-DeMo2";
  dob = new Date();
  money = 100;
  listOfProducts = [
    { name: "iPhone", price: "$999" },
    { name: "iMac", price: "$1999" },
    { name: "iWatch", price: "$399" },
    { name: "iPad", price: "$299" }
  ];
  testDataString = "Welcome to Edureka !";
*/
  employees = [
    {
      firstName: "Lois",
      lastName: "Walker",
      email: "lois.walker@hotmail.com",
      DOB: "03/29/1981",
      DateofJoining: "11/24/2003",
      salary: 168251
    },
    {
      firstName: "Lois",
      lastName: "Walker",
      email: "lois.walker@hotmail.com",
      DOB: "03/29/1981",
      DateofJoining: "11/24/2003",
      salary: 51063
    },
    {
      firstName: "Lois",
      lastName: "Walker",
      email: "lois.walker@hotmail.com",
      DOB: "03/29/1981",
      DateofJoining: "11/24/2003",
      salary: 50155
    },
    {
      firstName: "Lois",
      lastName: "Walker",
      email: "lois.walker@hotmail.com",
      DOB: "03/29/1981",
      DateofJoining: "11/24/2003",
      salary: 180294
    },
    {
      firstName: "Lois",
      lastName: "Walker",
      email: "lois.walker@hotmail.com",
      DOB: "03/29/1981",
      DateofJoining: "11/24/2003",
      salary: 117642
    },
    {
      firstName: "Lois",
      lastName: "Walker",
      email: "lois.walker@hotmail.com",
      DOB: "03/29/1981",
      DateofJoining: "11/24/2003",
      salary: 72305
    },
  ];
}

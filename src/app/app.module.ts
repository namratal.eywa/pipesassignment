import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyownuppercasePipe} from './pipes/myownuppercase.pipe';
import { GradefilterPipe } from './pipes/gradefilter.pipe';
import { MycustomedirectiveDirective } from './directives/mycustomedirective.directive';

@NgModule({
  declarations: [
    AppComponent,
    MyownuppercasePipe,
    GradefilterPipe,
    MycustomedirectiveDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
